import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';

import { AppComponent } from './app.component';
import { InputFormComponent } from './input-form/input-form.component';
import { DataTableComponent } from './data-table/data-table.component';
import { DirectiveNullDirective } from './directive-null.directive';
import { DirectivePatternDirective } from './directive-pattern.directive';

@NgModule({
   declarations: [
      AppComponent,
      InputFormComponent,
      DataTableComponent,
      DirectiveNullDirective,
      DirectivePatternDirective
   ],
   imports: [
      ReactiveFormsModule,
      BrowserAnimationsModule,
      BrowserModule,
      FormsModule,
      HttpClientModule,
      DropdownModule,
      TableModule,
      ButtonModule,
      DialogModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
