import { Directive } from '@angular/core';
import { NoNullValidator } from './input-form/input-form.component';
import { AbstractControl, NG_VALIDATORS } from '../../node_modules/@angular/forms';

@Directive({
  selector: '[appDirectiveNull]',
  providers: [{ provide: NG_VALIDATORS, useExisting: DirectiveNullDirective, multi: true }]

})
export class DirectiveNullDirective {

  constructor() { }
  private valNull = NoNullValidator();
  validate(control: AbstractControl): {[key: string]: any} {
    return this.valNull(control);
  }


}
