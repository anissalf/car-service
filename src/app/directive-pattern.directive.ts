import { Directive } from '@angular/core';
import { NoNullValidator } from './input-form/input-form.component';
import { AbstractControl, NG_VALIDATORS } from '../../node_modules/@angular/forms';

@Directive({
  selector: '[appDirectivePattern]',
  providers: [{ provide: NG_VALIDATORS, useExisting: DirectivePatternDirective, multi: true }]

})
export class DirectivePatternDirective {

  constructor() { }

}
