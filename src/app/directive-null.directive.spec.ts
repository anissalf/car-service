/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { DirectiveNullDirective } from './directive-null.directive';

describe('Directive: DirectiveNull', () => {
  it('should create an instance', () => {
    const directive = new DirectiveNullDirective();
    expect(directive).toBeTruthy();
  });
});
