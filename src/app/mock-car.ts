import { Car } from './Car';

export const CARS: Car[] = [
    { type: 1, vin: 'dsad231ff',	year: 2015,	    brand: 'VW'	 ,        color: 'Orange'},
    { type: 1, vin: 'gwregre345',   year: 2017,	    brand: 'Audi',	      color: 'Black'},
    { type: 1, vin: 'h354htr',	    year: 1998,	    brand: 'Renault',     color: 'Gray'},
    { type: 1, vin: 'j6w54qgh',     year: 2018,	    brand: 'BMW',	      color: 'Blue'},
    { type: 1, vin: 'hrtwy34',	    year: 1998,	    brand: 'Mercedes',    color: 'Orange'},
    { type: 1, vin: 'jejtyj',       year: 2015,	    brand: 'Volvo',	      color: 'Black'},
    { type: 1, vin: 'g43gr',	    year: 2016,	    brand: 'Honda',       color: 'Yellow'},
    { type: 1, vin: 'greg34',       year: 2017,	    brand: 'Jaguar',	  color: 'Orange'},
    { type: 1, vin: 'h54hw5',	    year: 2016,	    brand: 'Ford',        color: 'Black'},
    { type: 1, vin: '70214c7e',     year: 1998,     brand: 'Renault',     color: 'Black'},
    { type: 1, vin: 'ec229a92',     year: 1998,     brand: 'Audi',        color: 'Brown'},
    { type: 1, vin: '1083ee40',     year: 2018,     brand: 'VW',          color: 'Silver'},
    { type: 1, vin: '6e0da3ab',     year: 2016,     brand: 'Volvo',       color: 'Silver'},
    { type: 1, vin: '5aee636b',     year: 2015,     brand: 'Jaguar',      color: 'Maroon'},
    { type: 1, vin: '7cc43997',     year: 2017,     brand: 'Jaguar',      color: 'Orange'},
    { type: 1, vin: '88ec9f66',     year: 2018,     brand: 'Honda',       color: 'Maroon'},
    { type: 1, vin: 'f5a4a5f5',     year: 2017,     brand: 'BMW',         color: 'Blue'},
    { type: 1, vin: '15b9a5c9',     year: 2015,     brand: 'Mercedes',    color: 'Orange'},
    { type: 1, vin: 'f7e18d01',     year: 2016,     brand: 'Mercedes',    color: 'White'},
    ];
