/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { DirectivePatternDirective } from './directive-pattern.directive';

describe('Directive: DirectivePattern', () => {
  it('should create an instance', () => {
    const directive = new DirectivePatternDirective();
    expect(directive).toBeTruthy();
  });
});
