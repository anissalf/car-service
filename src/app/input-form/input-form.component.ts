import { Component, OnInit, Directive } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl } from '../../../node_modules/@angular/forms';
import { DataService } from '../data.service';

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.css']
})
export class InputFormComponent implements OnInit {

  kond = true;
  idx: number;
  public car: any[] = [];
  public tempData;
  public action = undefined;

  formInput = new FormGroup({
    vin: new FormControl(''),
    year: new FormControl(''),
    brand : new FormControl(''),
    color: new FormControl(''),
    prices: new FormControl(''),
    type: new FormControl('')
  });
  types: { label: string; value: string; }[];
  year: { label: string; value: string; }[];

  constructor( public carService: DataService ) { }

  ngOnInit() {
    // this.formInput = new FormGroup({
    //   'vin': new FormControl(this.formInput.vin, [
    //     Validators.required,
    //     Validators.minLength(4),
    //     // forbiddenNameValidator(/bob/i)
    //   ])
    // });
    this.types = [
      { label: 'Jeep', value: '1'},
      { label: 'Sedan', value: '2'},
      { label: 'Sport car', value: '3'}
    ];
    this.year = [
      { label: '1998', value: '1998'},
      { label: '2015', value: '2015'},
      { label: '2017', value: '2017'},
      { label: '2018', value: '2018'}
    ];

    this.carService.data2.subscribe($data2 => {
      if ($data2) {
        this.formInput.reset();
        this.formInput.patchValue($data2);
        this.action = 'update';
      } else {
        this.formInput.reset();
      }
    });
  }

  addCar() {
    if (this.action === 'update') {
      this.carService.updCar(this.formInput.value);
    } else {
      this.carService.addCar(this.formInput.value);
    }
    this.formInput.reset();
    this.carService.displayDialog = false;

  }

  reset() {
    this.formInput.reset();
    this.carService.displayDialog = false ;
  }


}

export function NoNullValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const isNull = (control.value === null || control.value === undefined || control.value === '' || control.pristine);
    const isValid = !isNull;
    return isValid ? null : { 'NoNull': 'value do not empty' };
  };
}

export function NoPatternValidator(regex = new RegExp('^[0-9]*$')): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
      const text = regex.test(control.value);
      return text ? null : { 'NoPattern': { value: control.value } };
  };
}
