import { Component, OnInit } from '@angular/core';
import { Car } from '../Car';
import { DataService } from '../data.service';


@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {
  cars: any[];
  cols: any[];
  newCar: boolean;
  idx: number;

  constructor ( public carService: DataService) {}

  ngOnInit() {
    this.carService.data.subscribe(newData => {
      this.cars = [...newData];
    });
    this.carService.getCars();
    this.cols = [
      { field: 'vin', header: 'Vin' },
      { field: 'year', header: 'Year' },
      { field: 'brand', header: 'Brand' },
      { field: 'color', header: 'Color' },
      { field: 'type', header: 'Type'}
    ];
  }

  getData(car) {
    this.carService.showDialogToAdd();
    this.carService.setData(car);
  }

  delCar(car) {
    this.carService.delCar(car.vin) ;
  }


}
