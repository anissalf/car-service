import { Injectable } from '@angular/core';
import { CARS } from './mock-car';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  kond = true;
  displayDialog = false;

  public tempData: any[] = CARS;
  public dataSource = new Subject<any>();
  public data = this.dataSource.asObservable();
  public dataSource2 = new Subject<any>();
  public data2 = this.dataSource2.asObservable();

  constructor( ) {
    this.getCars();
   }

  getCars(): void {
    this.dataSource.next(this.tempData);
  }

  getIndex(vin) {
    return this.tempData.indexOf(this.tempData.find(cr => cr.vin === vin));
  }

  delCar(vin): void {
    const i = this.getIndex(vin);
    this.tempData.splice(i, 1);
    this.getCars();
  }

  updCar(car) {
    this.displayDialog = true;
    this.tempData[this.getIndex(car.vin)] = car;
    this.getCars();
  }

  addCar(car) {
      this.tempData.push(car);
      return this.dataSource.next(this.tempData);
  }

  setData(car) {
    this.dataSource2.next(car);
  }

  showDialogToAdd() {
    this.kond = true;
    this.displayDialog = true;
  }

}
